API_ENTRY = "https://api.media.ccc.de/public/"

source = {
    id = "grl-mediacccde",
    name = "media.ccc.de",
    supported_keys = { "id", "url", "title", "description", "thumbnail", "publication-date", "duration", "size", "region", "mime-type" },
    supported_media = { "video" },
    tags = { "net:internet" },
    icon = "https://morr.cc/voctocat/voctocat.svg"
}


-- https://stackoverflow.com/questions/15706270/sort-a-table-in-lua
function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

local callbacks = 0
function callback_done()
    callbacks = callbacks - 1

    if callbacks == 0 then
        grl.callback()
    end
end

local media_list={}
local conference_callbacks = 0
function conference_callback_done(media)
    conference_callbacks = conference_callbacks - 1
    table.insert(media_list, media)

    if conference_callbacks == 0 then
        for _, m in spairs(media_list, function(t, a, b) return t[a].publication_date > t[b].publication_date end) do
            grl.callback(m, -1)
        end
        grl.callback()
    end
end

function conference_detail_fetch_cb(results)
    local json = {}
    json = grl.lua.json.string_to_table(results)

    local media = {}
    media.type = 'container'
    media.id = 'conferences/'..json.acronym
    media.title = json.title
    media.thumbnail = json.logo_url
    if json.event_last_released_at then
        media.publication_date = json.event_last_released_at
    else
        media.publication_date = json.updated_at:sub(1, 10)
    end

    media.childcount = #json.events

    conference_callback_done(media)
end

function api_entry_fetch_cb(results)
    local json = {}
    json = grl.lua.json.string_to_table(results)
    for i, conference in ipairs(json.conferences) do
        if conference.title then
            conference_callbacks = conference_callbacks + 1
        end
    end

    for i, conference in ipairs(json.conferences) do
        if conference.title ~= nil then
            grl.fetch(conference.url, conference_detail_fetch_cb)
        end
    end
end

function event_detail_fetch_cb(results)
    local json = {}
    json = grl.lua.json.string_to_table(results)
    local media = {}
    media.type = "video"
    media.id = "events/" .. json.guid
    media.title = json.title
    media.description = json.description
    media.thumbnail = json.thumb_url
    media.publication_date = json.release_date
    media.duration = json.duration
    media.size = json.size
    media.url = json.recordings[1].recording_url
    media.region = json.recordings[1].language
    media.mime_type = json.recordings[1].mime_type
    print(grl.lua.inspect(media))

    grl.callback(media, -1)

    callback_done()
end

function conference_fetch_cb(results)
    local json = {}
    json = grl.lua.json.string_to_table(results)
    for i, conference in ipairs(json.events) do
        if conference.title then
            callbacks = callbacks + 1
        end
    end

    for i, conference in ipairs(json.events) do
        if conference.title ~= nil then
            grl.fetch(conference.url, event_detail_fetch_cb)
        end
    end
end

function grl_source_browse(media_id)
    if not media_id then
        grl.fetch(API_ENTRY .. "conferences", api_entry_fetch_cb)
        return
    end
    print("Fetching id " .. media_id)

    if media_id:match("conferences/(.+)") then
        print("Fetching conference " .. media_id)
        grl.fetch(API_ENTRY .. media_id, conference_fetch_cb)
        return
    end

    grl.callback()
    return
end
